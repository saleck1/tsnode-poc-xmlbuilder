import { create } from 'xmlbuilder';
import axios from "axios";
import * as fs from "fs";

export class XMLConstructor {

  constructor()  {
    /*console.log(this.getBody(this.newsObj))
    console.log(this.getClose())

    console.log(this.getBody2(this.mrssObj))
    console.log(this.getClose2())*/


    this.test();


  }

  private async test() {

    fs.writeFile(__dirname + '/minified.xml', this.getBody(await this.jsonTOxml()).toString(), function (err) {
      if (err) throw err;
      console.log('Fichier créé !');
    });

    fs.writeFile(__dirname + '/beautified.xml', this.getBody(await this.jsonTOxml(), true).toString(), function (err) {
      if (err) throw err;
      console.log('Fichier créé !');
    });


    console.log(this.getBody(await this.jsonTOxml()))
    console.log(this.getClose())
    //console.dir(await this.jsonTOxml(), { depth: null, colors: true });
  }

  // Create header of XML
  private doc = create("notebook", { encoding: 'utf-8'});

  private doc2 = create("mrss", { encoding: 'utf-8', headless: true })

  // Insert content in xml
  private getBody = (body: any, pretty: boolean = false) => this.doc.ele(body).end({pretty: pretty});

  private getBody2 = (body: any) => this.doc2.ele(body);

  /*
   *  Close XML content, pretty property is optional
   *  but it's more easy to read. In production it's better.
   */
  private getClose = () => this.doc.end({ pretty: true });

  private getClose2 = () => this.doc2.end({ pretty: true });

  private newsObj ={
    'news':{
      'type':{
        '@type': 'daily news',
        'title':[
          {'@type': 'main', '#text': 'Peace In Ukraine'},
          {'@type': 'secondary', '#text': 'Vote In france soon'}]
      }
    }
  }

  private mrssObj ={
    'channel':{
      'title': "PEACH BLENDER",
      'link': "https://peach.blender.org/",
      'description': "Big Buck Bunny is a comedy about a well-tempered rabbit “Big Buck”, who finds his day spoiled by the rude actions of the forest bullies, three rodents. In the typical 1950ies cartoon tradition Big Buck then prepares for the rodents a comical revenge.",
      'item':{
        'title': "Big Buck Bunny",
        'link': "https://download.blender.org/peach/bigbuckbunny_movies/",
        'media:group':{
          'media:content':[
            {
              '@url': "https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4",
              '@fileSize': "64657027",
              '@type': "video/mp4",
              '@expression': "full",
              'media:player':{
                '@url': "https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4",
                '@width': "320",
                '@height': "180"},
              'media:rating': "nonadult",
              'media:category': "animation"
            },
            {
              '@url': "https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_640x360.m4v",
              '@fileSize': "121283919",
              '@type': "video/x-m4v",
              '@expression': "full",
              'media:player':{
                '@url': "https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_640x360.m4v",
                '@width': "640",
                '@height': "360"},
              'media:rating': "nonadult",
              'media:category': "animation"
            },
            {
              '@url': "https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_720p_h264.mov",
              '@fileSize': "416751190",
              '@type': "video/quicktime",
              '@expression': "full",
              'media:player':{
                '@url': "https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_720p_h264.mov",
                '@width': "1280",
                '@height': "720"},
              'media:rating': "nonadult",
              'media:category': "animation"
            },
            {
              '@url': "https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_1080p_stereo.avi",
              '@fileSize': "714744488",
              '@type': "video/x-msvideo",
              '@expression': "full",
              'media:player':{
                '@url': "https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_1080p_stereo.avi",
                '@width': "1920",
                '@height': "1080"},
              'media:rating': "nonadult",
              'media:category': "animation"
            }
          ],
          'media:thumbnail':[
            {
              '@url': "https://peach.blender.org/wp-content/uploads/bbb-splash.png",
              '@width': "1920",
              '@height': "1080"
            },
            {
              '@url': "https://peach.blender.org/wp-content/uploads/rodents.png",
              '@width': "1920",
              '@height': "1080"
            },
            {
              '@url': "https://peach.blender.org/wp-content/uploads/evil-frank.pn",
              '@width': "1920",
              '@height': "1080"
            },
            {
              '@url': "https://peach.blender.org/wp-content/uploads/bunny-bow.png",
              '@width': "1920",
              '@height': "1080"
            },
            {
              '@url': "https://peach.blender.org/wp-content/uploads/rinkysplash.jpg",
              '@width': "1920",
              '@height': "1080"
            },
            {
              '@url': "https://peach.blender.org/wp-content/uploads/its-a-trap.png",
              '@width': "1920",
              '@height': "1080"
            },
          ],
          'dcterms:valid':{
            '@start': "2022-03-18T15:14:37.521Z",
            '@end': "1679152477521",
            '@schema': "W3C-DTF"
          }
        }
      }
    }
  }


  private  jsonTOxml = async(): Promise<any>=>{
    const data=  await axios.get('https://jsonplaceholder.typicode.com/users/')
        .then(res=>res.data

        )
        .catch((err)=>{
          throw err
        })


    return {

          'entry':  data.map((e:any)=>({
            'user': {
              '@id': e.id, '@username': e.username, '@email': e.email, '#text': e.name
            },
            'address': {
              '@lat':e.address.geo.lat, '@lng': e.address.geo.lng, 'street': e.address.street, 'suite': e.address.suite, 'zipcode': e.address.zipcode, 'city': e.address.city
            },
            'contact': {
              'phone': e.phone,
              'website': e.website
            },
            'company':{
              '@catchPhrase': e.company.catchPhrase, '@bs': e.company.bs, '#text': e.company.name
            }
          }))



      }



    }






}
export default new XMLConstructor()
